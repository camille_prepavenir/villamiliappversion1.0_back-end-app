/* how our data are going to look like,
as a Reference point in order not to open every time Firebase */
let db = {
    users: [
        {
            userId: 'dh23ggj5h32g543j5gf43',
            email: 'user@email.com',
            handle: 'user',
            createdAt: '2020-05-11T13:51:42.709Z',
            imageUrl: 'image/dsfsdkfghskdfgs/dggdhfgdh',
// optionnal credential of basic user info to add for our front end 
            bio_biography_hostBio_hostShortStory: 'Hi, my name is user ! Nice to meet you !',
            website: 'https://user.com',
            location: 'London, UK'
        }
    ],
    comment_roomDisplay: [
        {
            userHandle: 'user55',
            roomId: 'btHLWs2i7UazgXJ1GXsE',
            body: 'this is a dummy room sample',
            createdAt: '2020-05-07T15:32:21.223Z',
// JS toISOString() Method: Return a Date object as a String, using the ISO standard: new Date().toISOString();
            likeCount: 5,
            commentCount: 2 //to minimize too many read on database to reduce the cost
        }
    ],
    comments_roomDetails: [
        {
            userHandle: 'user',
            roomId: 'btHLWs2i7UazgXJ1GXsE',
            body: 'nema bolje !',
            createdAt: '2020-05-07T15:32:21.223Z'
        }
    ],
    notifications: [
        {
            recipient: 'user',
            sender: 'johnDoe',
            read: 'true | false',
            roomId: 'btHLWs2i7UazgXJ1GXsE',
            type: 'like | comment',
            createdAt: '2020-05-07T15:32:21.223Z'
        }
    ]
};

const userDetails = {
    /* Redux data (user info hold in Redux state in our
    FrontEnd App, we're gonna use to populate our profile w/
1/ we need credential to show them on our profile on the right
2/ display the likes and display them or empty brackets) */
    credentials: {
        userId: 'N43KJ5H43KJHREW4J5H3JWERHB',
        email: 'user@email.com',
        handle: 'user',
        createdAt: '2020-05-07T15:32:21.223Z',
        imageUrl: 'image/dsfsdkfghskdfgs/dggdhfgdh',
        bio: 'Hi, my name is user ! Nice to meet you !',
        website: 'https://user.com',
        location: 'London, UK'
    },
    likes: [
        {
            userHandle: 'user',
            commentId: 'hh705oWfWucVzGbHH2pa'
        },
        {
            userHandle: 'user',
            commentId: 'hh705oWRDffdflijHH2pa'
        }
    ]
};


/* 1/ we keep the login route to min to increase response time
so first, we only get a token
then , we are re-/ directed to the home page
finally, we use this token in different route to get all details
f/ user 

2/The os module provides operating system-related utility methods and properties: https://nodejs.org/api/os.html

3/

*/

/* 

If error code	403
message	"Permission denied. Could not perform this operation"
while trying to acces image in firebase storage access rules
=> by default nothing's authorised unless known authentification
we're not using the client library but checking authentification
through a cloud fn() so we can allow read: coz
all files stock are user profile images which are public anyway; 

*/


/* if Error: 9 FAILED_PRECONDITION: The query requires an index.
You can create it here: https://console.firebase.google.com/v1/
=> 4complex firebase query we need to create an index for it */

