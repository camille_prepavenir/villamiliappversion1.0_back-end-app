### Villa Mili, Guesthouse

Where it feels like home !

VillaMili.hr, a Croatian website available in seven other languages for booking accommodation online, is aiming to be a travel metasearch engine for lodging reservations.

### Great Location

Featuring a garden, BBQ facilities and views of garden, Villa Mili is located in Hvar, 450 m from Beach Križna Luka. All rooms feature a kitchen and a private bathroom. The guest house features family rooms.

Guest rooms are equipped with air conditioning, a flat-screen TV with satellite channels, a fridge, a kettle, a shower, a hairdryer and a wardrobe. At the guest house, each room is equipped with a seating area.

Villa Mili offers a terrace.

Popular points of interest near the accommodation include Franciscan Monastery Beach, Pokonji Dol Beach and Nude Beach Stipanska.

### Host Information

We are welcoming you to our family home. Indeed, Villa Mili is a house with self-catering apartments. Each units are composed with a fully equipped kitchen and both private bathroom and balcony. Only the king size bed studio and the one bedroom apartment are sharing a terrace space. We are located approximately at 10-15 minutes from walking distance of Hvar's city center. Villa Mili is the place to Be as all your needs are covered; Such as a convenience store down the building.

Languages spoken: English, Spanish, Croatian

### House rules  

Smoking is not allowed.

Parties/events are not allowed.

Pets are not allowed. 

Quiet hours: Guests must be quiet between 23:00 and 07:00.

Refundable damage deposit: A damage deposit of EUR100 is required upon arrival, which is about HRK756.55. This deposit is fully refundable during check-out as long as there has been no damage to the property.

###  Check-in /  Check-out 

Check-in:   From 14:00 to 00:00 hours

Check-out:  Until 10:00 hours

### Cancellation / prepayment 

Cancellation and prepayment policies vary according to accommodation type. Please enter the dates of your stay and check the conditions of your required room. 

### Children and beds

Children of any age are welcome.

Children aged 12 years and above are considered adults at this property.

To see correct prices and occupancy information, please add the number of children in your group and their ages to your search.

0 - 2 years 	
Can use a cot upon request	FREE
Can use an extra bed upon request €20 per child, per night

3+ years 	
Can use an extra bed upon request €20 per person, per night

Supplements are not calculated automatically in the total costs and will have to be paid for separately during your stay.

The maximum number of extra beds and cots allowed is dependent on the room you choose. Please check your selected room for the maximum capacity.

All cots and extra beds are subject to availability.
