/* fn() which intercepts the request & according of its contents
will proceed towards our handle (stored in our authentification user
ie our collection created w/ const newComment) or stop there and send
a response (aka (piece of) middle-ware called here:
FBAuth as Firebase Authentification) */

/* implementing Auth middle-ware coz routes are not protected
making sure users are connected when submitting a post
(this functionnality has to be paired with handle ie 
get a token to make sure the request is authorised)*/

const { admin, db } = require('./admin');

module.exports = (req, res, next) => {
    let idToken;
    if (
        req.headers.authorization &&
        req.headers.authorization.startsWith('Bearer ')
    ) {
        idToken = req.headers.authorization.split('Bearer ')[1];
    } else {
        console.error('No token found');
        return res.status(403).json({ error: 'Unauthorized' });
    }

// verifing if token was issued by our app
    admin.auth().verifyIdToken(idToken)
        .then((decodedToken) => { 
            // holds the data inside our Token dat our req data has
            // added by our middleware (here user data)
            req.user = decodedToken;

            return db
                .collection('users')
                .where('userId', '==', req.user.uid)
                .limit(1) // limit the result to one document
                .get(); // let's run get
        })
       .then((data) => { //we get back data ie an one element array doc property
            req.user.handle = data.docs[0].data().handle;
            req.user.imageUrl = data.docs[0].data().imageUrl;

            // data() -> this fn() extract the data f/ the document
            // .handle -> property of the handle object in the users/user collection
            return next(); //allow the request to proceed toward next step
       })
       // once we have the setup, we have w/ the catch bloc
       // check if the token fail to be verified coz expired or blacklisted or other issuer
        .catch(err => {
            console.error('Error while verifying token', err);
            return res.status(403).json(err);
        });
};