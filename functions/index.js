const functions = require('firebase-functions');
const app = require('express')();
const FBAuth = require('./util/fbAuth');

const { db } = require('./util/admin');

const { 
    getAllComments, 
    postOneComment,
    getCommentDetails,
    commentOnRoom,
    likeRoom,
    unlikeRoom,
    deleteRoom
} = require('./handlers/comment');
const { 
    signup,
    login,
    uploadImage,
    addUserDetails,
    getAuthenticatedUser,
    getUserDetails,
    markNotificationsRead
} = require('./handlers/users');

// Comment routes
app.get('/comments', getAllComments); // not protected coz we're allowing user to view even if not logged in
app.post('/comment', FBAuth, postOneComment); // protected route so middleware
app.get('/room/:roomId', getCommentDetails); // the semi-colon tells the app in the url it's a route param
app.delete('/room/:roomId', FBAuth, deleteRoom);
app.get('/room/:roomId/like', FBAuth, likeRoom);
app.get('/room/:roomId/unlike', FBAuth, unlikeRoom);
app.post('/room/:roomId/comment', FBAuth, commentOnRoom);

// Users routes
app.post('/signup', signup);
app.post('/login', login);
app.post('/user/image', FBAuth, uploadImage); // protected route so middleware
app.post('/user', FBAuth, addUserDetails); // fn() for adding optionnal credential of basic user
app.get('/user', FBAuth, getAuthenticatedUser);
app.get('/user/:handle', getUserDetails);
app.post('/notifications', FBAuth, markNotificationsRead);

exports.api = functions.region('europe-west3').https.onRequest(app);


exports.createNotificationOnLike = functions
  .region('europe-west3')
  .firestore.document('likes/{id}')
  .onCreate((snapshot, context) => {
    /* const roomId = context.params.roomId;
    const batch = db.batch();
    return batch */
    return db
      .doc(`/room/${snapshot.data().roomId}`)
      .get()
      .then( doc => {
        if (
          doc.exists &&
          doc.data().userHandle !== snapshot.data().userHandle
        ) {
          return db.doc(`/notifications/${snapshot.id}`).set({
            // .batch.set(
            createdAt: new Date().toISOString(),
            recipient: doc.data().userHandle,
            sender: snapshot.data().userHandle,
            type: 'like',
            read: false,
            roomId: doc.id
          });
        }
      })
      .catch((err) => console.error(err));
  });
exports.deleteNotificationOnUnlike = functions
  .region('europe-west3')
  .firestore.document('likes/{id}')
  .onDelete((snapshot, context) => {
    return db
      .doc(`/notifications/${snapshot.id}`)
      .delete()
      .catch((err) => {
        console.error(err);
        return;
      });
  });
exports.createNotificationOnComment = functions
  .region('europe-west3')
  .firestore.document('comments/{id}')
  .onCreate((snapshot, context) => {
    return db
      .doc(`/room/${snapshot.data().roomId}`)
      .get()
      .then((doc) => {
        if (
          doc.exists &&
          doc.data().userHandle !== snapshot.data().userHandle
        ) {
          return db.doc(`/notifications/${snapshot.id}`).set({
            createdAt: new Date().toISOString(),
            recipient: doc.data().userHandle,
            sender: snapshot.data().userHandle,
            type: 'comment',
            read: false,
            roomId: doc.id
          });
        }
      })
      .catch((err) => {
        console.error(err);
        return;
      });
  });

exports.onUserImageChange = functions.region('europe-west3').firestore.document('/users/{userId}')
    .onUpdate((change, context) => {
      console.log(change.before.data());
      console.log(change.after.data());
      if(change.before.data().imageUrl !== change.after.data().imageUrl){
       console.log('image has changed');
        const batch = db.batch();

      return db.collection('room').where('userHandle', '==', change.before.data().handle).get()
        .then((data) => {
          data.forEach(doc => {
            const room = db.doc(`/room/${doc.id}`);
            batch.update(room, { userImage: change.after.data().imageUrl});
          })
          return batch.commit();
        });
      } else return true;
    });

exports.onRoomDelete = functions.region('europe-west3').firestore.document('/room/{roomId')
    .onDelete((snapshot, context) => {
      const roomId = context.params.roomId;
      const batch = db.batch();
      return db.collection('comments').where('roomId', '==', roomId).get()
        .then(data => {
          data.forEach(doc => {
            batch.delete(db.doc(`/comments/{doc.id}`));
          });
          return db.collection('likes').where('roomId', '==', roomId).get();   
        })
        .then(data => {
          data.forEach(doc => {
            batch.delete(db.doc(`/likes/{doc.id}`));
          })
          return db.collection('notifications').where('roomId', '==', roomId).get();     
        })
        .then(data => {
          data.forEach(doc => {
            batch.delete(db.doc(`/notifications/{doc.id}`));
          })
          return batch.commit();            
        })
        .catch(err => console.error(err));
    })