const { admin, db } = require('../util/admin');

const firebaseConfig = require('../util/firebaseConfig');

const firebase = require('firebase');
firebase.initializeApp(firebaseConfig)

const { validateSignupData, validateLoginData, reduceUserDetails } = require('../util/validators');

//sign users in
exports.signup = (req, res) => {
    /*  verifing if datas are valid before implementing into our database
    meaning: newUser fields are not empty and [pwd & confirmPwd matched together] */
    const newUser = {
        email: req.body.email,
        password: req.body.password,
        confirmPwd: req.body.confirmPwd,
        handle: req.body.handle
    };

    // destructuring ie extracting valid & errors f/ validateSignupData
    const { valid, errors } = validateSignupData(newUser);
    if (!valid) return res.status(400).json(errors);

    // by default, whenever a user signs up we're adding an imageUrl with no face
    const noImg = 'no-img.png';

     // TODO: validate data (done below!)
    let token, userId;
    db.doc(`/users/${newUser.handle}`).get()
        .then(doc => {
            if(doc.exists) {
                return res.status(400).json({ handle: 'this handle is already taken'});
            } else {     //signup our user:
                return firebase
                .auth()
                .createUserWithEmailAndPassword(newUser.email, newUser.password)
                }
            })
            .then(data => { // access token for user to use to request data
               userId = data.user.uid;
               return data.user.getIdToken();
            })
            .then(idToken => { // name of property and value are the same: token (named below once)
                token = idToken;
                const userCredentials = { // creating user document
                    handle: newUser.handle,
                    email: newUser.email,
                    createdAt: new Date().toISOString(),
                    imageUrl: `https:\\firebasestorage.googleapis.com/v0/b/${
                        firebaseConfig.storageBucket
                    }/o/${noImg}?alt=media`,
                    userId
                }; // set these info into our firebase users collections
                return db.doc(`/users/${newUser.handle}`).set(userCredentials);
            })
            .then( () => {
                return res.status(201).json({ token });
            })
            .catch(err => {
            console.error(err);
            if(err.code === 'auth/email-already-in-use'){
                return res.status(400).json({ email: 'Email is already in use'});
            } else {
                return res.status(500).json({ general: "Something went wrong, please try again" });
            }
            });
};

// log user in
exports.login = (req, res) => {
    const user = {
        email: req.body.email,
        password: req.body.password
    };

    // destructuring ie extracting valid & errors f/ validateSignupData
    const { valid, errors } = validateLoginData(user);  

    if (!valid) return res.status(400).json(errors);

    firebase
        .auth()
        .signInWithEmailAndPassword(user.email, user.password)
        .then((data) => {
            return data.user.getIdToken();
        })
        .then((token) => {
            return res.json({ token });
        })
        .catch((err) => {
            console.error(err);
            // auth/wrong-password
            // auth/user-not-user
            return res
              .status(403)
              .json({ general: "Wrong credentials, please try again" });
      });
};

// Add user Details
exports.addUserDetails = (req, res) => {
    let userDetails = reduceUserDetails(req.body);

    db.doc(`/users/${req.user.handle}`).update(userDetails)
        .then(()=> {
            return res.json({ message: 'Details added successfully'});
        })
        .catch(err=> {
            console.error(err);
            return res.status(500).json({ error: err.code});
        });
};

// Get own authentificated user details
exports.getAuthenticatedUser = (req,res) => {
    let userData = {}; // response data will be added to it as we go through our promess chain
    //first we need to get the user
    db.doc(`/users/${req.user.handle}`).get()
        .then(doc => {
            if(doc.exists){
                userData.credentials = doc.data();
                return db.collection('likes').where('userHandle', '==', req.user.handle).get()
            }
        })
        .then(data => {
            userData.likes = []; // we need to initialize it coz without it gonna mess up with types, & will crash
            data.forEach(doc => {
                userData.likes.push(doc.data());
            });
            return res.json(userData);
            /* return db.collection('notifications').where('recipient' '==', req.user.handle)
                .orderBy('createdAt', 'desc').limit(10).get(); */
        })
        /* .then(data => {
            userData.notifications = [];
            data.forEach.(doc => {
                userData.notifications.push({
                    recipient: doc.data().recipient,
                    sender: doc.data().sender,
                    createdAt: doc.data().createdAt,
                    roomId: doc.data().roomId,
                    type: doc.data().type,
                    read: doc.data().read,
                    notificationId: doc.id
                })
            });
            return res.json(userData);
        }) */
        .catch(err => {
            console.error(err);
            return res.status(500).json({ error : err.code});
        });
};

// Upload a profile image for user
exports.uploadImage = (req, res) => {
    const   BusBoy = require('busboy'), // A streaming parser package: on event on file upload
            path = require('path'), // default package on node project
            os = require('os'), // provides operating system-related utility methods and properties.
            fs = require('fs'); // stand for file system

    const busboy = new BusBoy({ headers: req.headers });

    let imageFileName, imageToBeUploaded = {};

    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
        console.log(fieldname, file, filename, encoding, mimetype);
        if(mimetype !== 'image/jpep' && mimetype !== 'image/png') {
            return res.status(400).json({ error: 'Wrong file type submitted' }); //400 for bad request
         }
        // split by dots if my.image.png
        const imageExtension = filename.split('.')[filename.split('.').length -1];
        // we try to get 564843835278.png
        imageFileName = `${Math.round(Math.random()*1000000000000)}.toString()}.${imageExtension}`;
        const filepath = path.join(os.tmpdir(), imageFileName);
        // creating the image to be uploaded
        imageToBeUploaded = { filepath, mimetype };
        // need the filesystem librairy to create this file
        file.pipe(fs.createWriteStream(filepath));
    });
    busboy.on('finish', () => {
        // uploading the file just created
        admin
            .storage()
            .bucket()
            .upload(imageToBeUploaded.filepath, {
                resumable: false,
                metadata: {
                    metadata: {
                        contentType: imageToBeUploaded.mimetype
                    }
             }
            }) // constructing the image url to add to our user
        .then(() => {
            const imageUrl = `https:\\firebasestorage.googleapis.com/v0/b/${firebaseConfig.storageBucket}/o/${imageFileName}?alt=media`
            // adding to our user document -> authentificated via MW so access to user object
            return db.doc(`/users/${req.user.handle}`).update({ imageUrl });
        })
        .then(() => {
            return res.json({ message: 'Image uploaded successfully'});
        })
        .catch(err => {
            console.error(err);
            return res.status(500).json({ error: err.code });
        });
    });
    busboy.end(req.rawBody);
};
// Get any user's details
exports.getUserDetails = (req, res) => {
    let userData = {};
    db.doc(`/users/${req.params.handle}`).get()
        .then(doc => {
            if(doc.exists){
                userData.user = doc.data();
                return db.collection('room')
                    .where('userHandle', '==', req.params.handle)
                    .orderBy('createdAt', 'desc')
                    .get();
            } else {
                return res.status(404).json({ error: 'User not found'});
            }
        })
        .then(data => {
            userData.rooms = [];
            data.forEach(doc => {
                userData.rooms.push({
                    body: doc.data().body,
                    createdAt: doc.data().createdAt,
                    userHandle: doc.data().userHandle,
                    userHandle: doc.data().userHandle,
                    userImage: doc.data().userImage,
                    likeCount: doc.data().likeCount,
                    commentCount: doc.data().commentCount,
                    roomId: doc.id
                })
            });
            return res.json(userData);
        })
        .catch(err => {
            console.error(err);
            return res.status(500).json({ error: err.code  });
        });
};

exports.markNotificationsRead = (req, res) => {
    // firebase batch to update multiple doc
    let batch = db.batch();
    req.body.forEach(notificationId => {
        const notification = db.doc(`/notifications/${notificationId}`);
        batch.update(notification, {read: true});
    });
    batch
        .commit()
        .then( () => {
            return res.json({ message: 'Notifications marked read'});
        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({ error: err.code });
        });
};