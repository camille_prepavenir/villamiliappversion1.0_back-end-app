const { admin, db } = require('../util/admin');

exports.getAllComments = (req, res) => {
    admin
        .firestore()
        .collection('comment')
        .orderBy('createAt', 'desc')
        .get()
        .then(data => {
            let comment = [];
            data.forEach(doc => {
                comment.push({
                commentId: doc.id,
                body: doc.data().body,
                commentCount: doc.data().commentCount,
                likeCount: doc.data().likeCount,
                userImage: doc.data().userImage,               
                userHandle: doc.data().userHandle,
                createAt: doc.data().createAt
                });
            });
            return res.json(comment);
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });
};

exports.postOneComment = (req, res) => {
    /*  express automatically takes care of:
      if(req.method !== 'POST'){
          return res.status(400).json({ error: 'Method not allowed'});
      } */
      const newComment = {
          body: req.body.body,
          userHandle: req.user.handle, // f/ FBAuth
          userImage: req.user.imageUrl,
          createAt: new Date().toISOString(),
          likeCount: 0,
          commentCount: 0          
      };
  
      db
          .collection('comment')
          .add(newComment)
          .then(doc => {
              const resRoom = newComment;
              resRoom.roomId = doc.id;
              res.json(resRoom);
              //return res.json({ message: `document ${doc.id} created successfully`});
          })
          .catch(err => {
                console.error(err);
                res.status(500).json({ error: 'something went wrong'});
          });
  };

  // Fetch one Room
  exports.getCommentDetails = (req, res) => {
    let commentData = {};
    db.doc(`/room/${req.params.roomId}`).get()
        .then((doc) => {
            if (!doc.exists) {
                return res.status(404).json({ error: 'Room not found'}); //if ID does not exist anymore
            }
            commentData = doc.data();
            commentData.roomId = doc.id;
            return db
                .collection('comments')
                .orderBy('createdAt', 'desc')
                .where('roomId', '==', req.params.roomId)
                .get();
        })
        .then((data) => {
            commentData.comments = []; // can be multiple documents
            data.forEach((doc) => {
                commentData.comments.push(doc.data());
            });
            return res.json(commentData);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).json({ error: err.code});
        });
  };

  //Comment on a room 
  exports.commentOnRoom = (req, res) => {
      if(req.body.body.trim() === '')
      return res.status(400).json({ comment: 'Must not be empty'});

      const newComment = {
          body: req.body.body,
          createdAt: new Date().toISOString(),
          roomId: req.params.roomId,
          userHandle: req.user.handle,
          userImage: req.user.imageUrl
      };
// to not persist comments to unexisting room
        db.doc(`/room/${req.params.roomId}`).get()
            .then(doc => {
                if(!doc.exists){
                    return res.status(404).json({ error: 'Room not found'});
                }
                return doc.ref.update({ commentCount: doc.data().commentCount + 1});
            })
            .then(() => {
                return db.collection('comments').add(newComment);
            })
            .then(() => {
                res.json(newComment);
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({ error: 'Something went wrong' });
            });
};

// Like a room
exports.likeRoom = (req, res) => {
    const likeDocument = db.collection('likes').where('userHandle', '==', req.user.handle)
        .where('roomId', '==', req.params.roomId).limit(1); // to get an array of one document

    const roomDocument = db.doc(`/room/${req.params.roomId}`);

    let roomData;

    roomDocument.get()
        .then(doc => {
            if(doc.exists){
                roomData = doc.data();
                roomData.roomId = doc.id;
                return likeDocument.get();
            } else {
                return res.status(404).json({ error: 'Room not found'});
            }
        })
        .then(data => {
            if(data.empty){
                return db.collection('likes').add({
                    roomId: req.params.roomId,
                    userHandle: req.user.handle
                })
/* we can't return and handle the promise in the next then, coz
even if not empty it might actually go throught. So we need to
nest the .then inside of the if block to avoid dat issue */
                .then(() => {
                    roomData.likeCount++
                    return roomDocument.update({ likeCount: roomData.likeCount });
                })
                .then(() => {
                   return res.json(roomData); 
                })
            } else {
                return res.status(400).json({ error: 'Room already liked'});
            }
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({ error: err.code});
        });
};

// Unlike a room
exports.unlikeRoom = (req, res) => {
    const likeDocument = db.collection('likes').where('userHandle', '==', req.user.handle)
        .where('roomId', '==', req.params.roomId).limit(1); // to get an array of one document

    const roomDocument = db.doc(`/room/${req.params.roomId}`);

    let roomData;

    roomDocument.get()
        .then(doc => {
            if(doc.exists){
                roomData = doc.data();
                roomData.roomId = doc.id;
                return likeDocument.get();
            } else {
                return res.status(404).json({ error: 'Room not found'});
            }
        })
        .then(data => {
            if(data.empty){
                return res.status(400).json({ error: 'Room not liked'});
            } else {
                return db.doc(`/likes/${data.docs[0].id}`).delete()
                    .then(() => {
                        roomData.likeCount--;
                        return roomDocument.update({ likeCount : roomData.likeCount })
                    })
                    .then(() => {
                        res.json(roomData);
                    })
                }
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({ error: err.code});
        });
};

// Delete a room
exports.deleteRoom = (req, res) => {
    const document = db.doc(`/room/${req.params.roomId}`);
    document.get()
        .then(doc => {
            if(!doc.exists){
                return res.status(404).json({ error: 'Room not found'});
            } // making sure the user is the owner
            if(doc.data().handle !== req.user.handle){
                return res.status(403).json({ error: 'Unauthorized'});
            } else {
                return document.delete();
            }
        })
        .then(() => {
            res.json({ message: 'Room deleted successfully'});
        })
        .catch(err => {
            console.error(err);
            return res.status(500).json({ error: err.code });
        })
}